<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$idArticle = $app->input->getCmd('id', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
if ($task == "edit" || $layout == "form") {
    $fullWidth = 1;
} else {
    $fullWidth = 0;
}
// Add Stylesheets
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/bootstrap.min.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/font-awesome.min.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/navbar_menu.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/menus.css');
// Add scripts
JHtml::_('jquery.framework');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/popper.min.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/bootstrap.min.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/smoothscroll.js');
$icon = $this->baseurl;
// Adjusting content width
if ($this->countModules('sidebar-left') && $this->countModules('sidebar-right')) {
    $span = "col-md-6";
} elseif ($this->countModules('sidebar-left') && !$this->countModules('sidebar-right')) {
    $span = "col-md-9";
} elseif (!$this->countModules('sidebar-left') && $this->countModules('sidebar-right')) {
    $span = "col-md-9";
} else {
    $span = "col-md-12";
}

$ImagenDireccion = $params->get('ImagenDireccion');
$ImagenTelefono = $params->get('ImagenTelefono');
$ImagenFacebook = $params->get('ImagenFacebook');
$ImagenTwitter = $params->get('ImagenTwitter');
$ImagenPagina = $params->get('ImagenPagina');
$ImagenCorreo = $params->get('ImagenCorreo');


$URLFacebook = $params->get('URLFacebook');
$URLTwitter = $params->get('URLTwitter');
$URLPagina = $params->get('URLPagina');

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <jdoc:include type="head" />
        <?php if($this->params->get('favicon')) { ?>
            <link rel="shortcut icon" href="<?php echo JUri::root(true) . htmlspecialchars($this->params->get('favicon'), ENT_COMPAT, 'UTF-8'); ?>" />
        <?php } ?>
        <!--[if lt IE 9]>
                <script src="<?php //echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
        <![endif]-->
        <style type="text/css">
            .logo{
                width: 300px;
            }
            #logoceo{
                margin-top: 5%;
                margin-left: 36%;
            }
            #menus{
                border-bottom:4px solid #128ad6;
                border-top:4px solid #128ad6;
                background-color: #003864;
                position: relative;
            }
            .logoMenu{
                position: absolute;
                margin-top: -28vh;
                margin-left: 15%;
                height: 60vh;
                width: 60vh;
            }
            @media(min-width: 1601px){
                #logoceo{
                    margin-top: 5%;
                    margin-left: 45%;
                }
            }
            @media(max-device-width: 768px){
               #logoceo{
                    margin-top: 15%;
                    margin-left: 10%;
                }
            }
            @media(max-width: 1500px){
                #footer_logo_i{
                    width: 50%;
                    margin-top:15em;
                }
                #logoceo {
                    margin-left: 36%;
                }
            }
            @media(max-width: 575px){
                #logoceo {
                    margin-left: 28%;
                }
                #footer_logo_i{
                    width: 600px;
                }
            }
            @media(max-device-width: 400px){
                #logoceo{
                    margin-top: 25%;
                    margin-left: 5%;
                }
                #footer_logo_i{
                    width: 600px;
                }
            }
        </style>
    </head>
    <body>
        <header class="header">
            <div class="container-fluid">
                <div class="top-encabezado row"></div> 
            </div>
            <?php if ($this->countModules('menu')) : ?>
                <div class="container-fluid" id="menus">
                    <div class="col-sm-12" style="width: 80%;margin-left: 20%;margin-right: 20%">
                        <img src="<?php echo JURI::base() . '/templates/' . $this->template ; ?>/images/logo.png" class="logoMenu">
                        <jdoc:include type="modules" name="menu" style="xhtml" />
                    </div>
                </div>
            <?php endif; ?>
            <!--<?php if ($this->countModules('banner')) : ?>
                <div class="row">
                    <div class="col-sm-12">
                        <jdoc:include type="modules" name="banner" style="xhtml" />
                    </div>
                </div>
            <?php endif; ?>-->
            <?php if ($this->countModules('animacion')) : ?>
                <div class="row">
                    <div class="col-sm-12">
                        <jdoc:include type="modules" name="animacion" style="xhtml" />
                    </div>
                </div>
            <?php endif; ?>
        </header>
        <div class="body">
            <div class="content">
                <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
                            <div class="modulos col-sm-12">
                                <jdoc:include type="modules" name="content" style="xhtml" />
                            </div>
                       <!--  <jdoc:include type="message" /> --> 
                       <?php 
                        $idArticle = (int) $idArticle;
                        if($view == "article" && $idArticle > 41){
                        ?> 
                        <jdoc:include type="component" />
                        <?php } ?> 
                    <div class="row" id="contacto" style="margin-top:2%;">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-xs-1 col-sm-1 col-lg-1" id="footer_logo_i">
                                    <!-- <div class="ic-princ">
                                        <div><img src="<?php //echo $this->baseurl . '/templates/' . $this->template ; ?>/images/triangulo.png"></div>
                                    </div> -->
                                </div> 
                                <div id="conac">
                                    <!-- <div class="caja-contacto">
                                      <div><a href="<?php echo $URLFacebook;?>"><img src="<?php echo $ImagenFacebook;?>"><p><?php echo ($params->get('Facebook')); ?></p></a></div>
                                        <div><a href="<?php echo $URLTwitter;?>"><img src="<?php echo $ImagenTwitter;?>"><p><?php echo ($params->get('Twitter')); ?></p></a></div>
                                        <div><a href="<?php echo $URLPagina;?>"><img src="<?php echo $ImagenPagina;?>"><p><?php echo ($params->get('Pagina')); ?></p></a></div>
                                        <div><img src="<?php echo $ImagenCorreo;?>"><p><?php echo ($params->get('Correo')); ?></p></div>
                                    </div> -->
                                </div>
                                <div class="col-xs-3 col-sm-3 col-lg-3" id="footer_logo_d">
                                </div>
                                <div class="col-xs-4 col-sm-4 col-lg-4" style="margin-left:3%">
                                    <jdoc:include type="modules" name="footer" style="xhtml" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
        </footer>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-128913995-1"></script>
        <script>
        
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-128913995-1');
        </script>
    </body>
</html>
