<?php
// No direct access
defined('_JEXEC') or die();

$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::base() . 'modules/mod_nosotros/tmpl/style.css');
$doc->addScript(JUri::base() . 'modules/mod_nosotros/tmpl/main.js');
$imagen = json_decode($module->params);
$i = JUri::base() . $imagen->imagen;
$ima_foot = JUri::base().'/templates/ikots';

?>

<div class="row mod-nosotros">
<?php 
	foreach ( $nosotros as $key => $item ) { 
?>

 <div id="<?php echo $item->alias; ?>" class="col-lg-12 col-md-12 col-sm-12">
 	<div class="imagen_banner col-sm-12 col-md-12 col-lg-12">
		<img src="<?php echo $i; ?>" alt="" class="img-titular">
	</div>
	<h1><?php echo $item->title ; ?></h1>
 	<div class="content menuTexto">
 		<?php echo (strlen($item->fulltext) > strlen($item->introtext))? $item->fulltext : $item->introtext; ?>
 	</div>

 </div>
<?php 
	}
?>
	<div class="imagen_footer col-sm-12 col-md-12 col-lg-12">
		<img src="<?php echo $ima_foot; ?>/images/FooterNosotros.png" alt="" class="img-footer">
	</div>
</div>