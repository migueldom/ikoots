(function(window, $){
	$(function(){
		let $container = $('.mod-nosotros');
		let titlesLeft = $container.find('.left h1');
		let titlesRight = $container.find('.right h1');
		
		for (var i = 0; i <= titlesLeft.length - 1; i++) {
			let title = titlesLeft[i].innerHTML.split(" ");
			let content = '';
			for (var j = 0; j <= title.length - 1; j++) {
				content += '<span>' + title[j] + '</span> ';
			}
			titlesLeft[i].innerHTML = content;
		}

		for (var i = 0; i <= titlesRight.length - 1; i++) {
			let title = titlesRight[i].innerHTML.split(" ");
			let content = '';
			for (var j = 0; j <= title.length - 1; j++) {
				content += '<span>' + title[j] + '</span> ';
			}
			titlesRight[i].innerHTML = content;
		}		
	});
})(window, jQuery);