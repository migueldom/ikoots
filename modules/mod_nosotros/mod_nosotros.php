<?php 
// No direct access
defined('_JEXEC') or die();
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

$nosotros = modNosotrosHelper::getData($params);

require JModuleHelper::getLayoutPath('mod_nosotros');
