<?php 

/**
 *  Helper class for Nosotros Module
 * @subpackage Modules
 * 
 */
class ModNosotrosHelper
{
	public static function getData($params){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('*')
			->from($db->quoteName('#__content'))
			->where($db->quoteName('catid') . ' = ' . $params->get('categoria', 0));
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		return $rows;
	}
}